﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using NativeUI;

namespace mxfivereborn
{
    public class ShopBase
    {
        //
        // Types
        //
        public enum Status
        {
            None = 0,
            Entering = 1,
            Inside = 2,
            Exiting = 3
        }

        public class ItemCallbackInfo
        {
            public uint Hash;
            public int Cost;
            public ItemCallbackInfo(uint hash, int cost) { Hash = hash; Cost = cost; }
        }

        public class Location
        {
            public float TriggerRadius { get; private set; }
            public Vector3 Trigger { get; private set; }
            public Vector3 Active { get; private set; }
            public Vector3 Leave { get; private set; }

            public Location(float triggerRadius, Vector3 trigger, Vector3 active, Vector3 leave)
            {
                TriggerRadius = triggerRadius;
                Trigger = trigger;
                Active = active;
                Leave = leave;
            }
        }

        //
        // Fields
        //
        public delegate void OwnershipCheck(UIMenu sender, UIMenuItem item);
        public delegate void BuyEvent(string name, ItemCallbackInfo info);
        public delegate void EnteringShopEvent();
        public delegate void EnteredShopEvent();
        public delegate void ExitingShopEvent();
        public delegate bool AllowEntryCheck();

        MenuPool MenuPool;
        Dictionary<string, ItemCallbackInfo> ItemInfo;
        public List<Location> Locations;
        public UIMenu MainMenu;
        public OwnershipCheck CheckOwnership;
        public BuyEvent Buy;
        public EnteringShopEvent EnteringShop;
        public EnteredShopEvent EnteredShop;
        public ExitingShopEvent ExitingShop;
        public AllowEntryCheck AllowEntry;
        public Status status { get; private set; }

        //
        // Methods
        //
        public void Open() { if (!MenuPool.IsAnyMenuOpen()) MainMenu.Visible = true; }
        public void Close() { MenuPool.CloseAllMenus(); }

        public ShopBase(string name, string description = "")
        {
            Locations = new List<Location>();

            MenuPool = new MenuPool();
            MainMenu = new UIMenu(name, description);
            MainMenu.MouseControlsEnabled = false;
            MenuPool.Add(MainMenu);
            MenuPool.RefreshIndex();

            ItemInfo = new Dictionary<string, ItemCallbackInfo>();

            CheckOwnership = (sender, item) => { /*Util.Print("Default Ownership Check.");*/ };
            Buy = (itemName, info) => { /*Util.Print("Default Buy Event.");*/ };
            EnteringShop = () => { /*Util.Print("Default EnterShop Event.");*/ };
            EnteredShop = () => { /*Util.Print("Default EnterShop Event.");*/ };
            ExitingShop = () => { /*Util.Print("Default ExitShop Event.");*/ };
            AllowEntry = () => { return true; };
        }

        public void Tick()
        {
            MenuPool.ProcessMenus();

            if (status == Status.None)
            {
                foreach (Location loc in Locations)
                {
                    float dist = World.GetDistance(Game.PlayerPed.Position, loc.Trigger);
                    if (dist <= loc.TriggerRadius && AllowEntry())
                    {
                        EnteringShop();
                        status = Status.Entering;
                        MainMenu.Visible = true;
                    }
                }
            }
            else if (status == Status.Inside)
            {
                if (!MenuPool.IsAnyMenuOpen())
                {
                    ExitingShop();
                    status = Status.Exiting;
                }
            }
        }

        public void SortAllMenus()
        {
            MainMenu.MenuItems.Sort((x, y) => x.Text.CompareTo(y.Text));

            MenuPool.ToList().ForEach(new Action<UIMenu>((target) =>
            {
                target.MenuItems.Sort((x, y) => x.Text.CompareTo(y.Text));
            }));

            MenuPool.RefreshIndex();
        }

        public UIMenu AddCategory(string name, string description = "")
        {
            return AddCategory(MainMenu, name, description);
        }

        public UIMenu AddCategory(UIMenu parent, string name, string description = "")
        {
            UIMenu categoryMenu = MenuPool.AddSubMenu(parent, name, description);
            categoryMenu.MouseControlsEnabled = false;
            categoryMenu.MouseEdgeEnabled = false;
            categoryMenu.OnItemSelect += (sender, item, index) =>
            {
                // if player money >= Shop.GetItemInfoFromKey(item.Text).Cost
                // reduce player money
                ItemCallbackInfo info = GetItemInfoFromKey(item.Text);

                Buy(item.Text, info);
            };
            categoryMenu.OnMenuClose += (sender) => { sender.ParentMenu.Visible = true; };

            MenuPool.RefreshIndex();
            return categoryMenu;
        }

        public UIMenu GetCategoryMenu(UIMenu parent, string name)
        {
            UIMenuItem item = parent.Children.Keys.ToList().Find(i => i.Text == name);
            UIMenu menu = null;

            if (item != null)
                menu = parent.Children[item];

            return menu;
        }

        public UIMenuItem AddItem(UIMenu parent, string name, string description, uint hash, int cost)
        {
            // Make sure the item is unique, force it if not
            if (ItemInfo.ContainsKey(name))
            {
                for (int i = 1; i < 100; i++)
                {
                    var tmpString = $"{name} Alternative {i}";
                    if (!ItemInfo.ContainsKey(tmpString))
                    {
                        name = tmpString;
                        break;
                    }
                }
            }

            UIMenuItem item = new UIMenuItem(name, description);
            parent.AddItem(item);

            ItemInfo.Add(name, new ItemCallbackInfo(hash, cost));

            MenuPool.RefreshIndex();

            return item;
        }

        public ItemCallbackInfo GetItemInfoFromKey(string key)
        {
            ItemCallbackInfo item;

            if (!ItemInfo.TryGetValue(key, out item))
                return null;

            return item;
        }
    }
}
