﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using NativeUI;
using CitizenFX.Core.UI;
using CitizenFX.Core.Native;
using System.Drawing;

namespace mxfivereborn
{
    class VehicleShop : BaseScript
    {
        private ShopBase Shop;
        private bool Initialized = false;

        public VehicleShop()
        {
            Shop = new ShopBase("Vehicle Shop", "~b~Sick Wheels");
            Shop.Buy = async (name, info) =>
            {
                Vehicle vehicle = await World.CreateVehicle((VehicleHash)info.Hash, LocalPlayer.Character.Position, LocalPlayer.Character.Heading);

                vehicle.IsInvincible = false;
                vehicle.IsEngineRunning = true;

                if (LocalPlayer.Character.IsInVehicle())
                    LocalPlayer.Character.CurrentVehicle.Delete();

                LocalPlayer.Character.SetIntoVehicle(vehicle, VehicleSeat.Driver);

                Shop.Close();
            };

            Tick += OnTick;
        }

        private void LoadShopMenus()
        {
            foreach (Model veh in Util.GetEnumValues<VehicleHash>())
            {
                VehicleClass vehClass = Util.GetVehicleClass(veh);
                string vehClassString = Util.GetVehicleClassName(vehClass);
                UIMenu curMenu = Shop.GetCategoryMenu(Shop.MainMenu, vehClassString);

                if (curMenu == null)
                    curMenu = Shop.AddCategory(vehClassString);

                string vehName = Util.GetVehicleName(veh);

                UIMenuItem item = Shop.AddItem(curMenu, vehName, "", (uint)veh.Hash, 0);
            }

            Shop.SortAllMenus();
        }

        private async Task OnTick()
        {
            Shop.Tick();

            if (Game.IsControlPressed(Game.CurrentInputMode == InputMode.GamePad ? 0 : 1, Control.MultiplayerInfo))
            {
                if (!Initialized)
                {
                    LoadShopMenus();
                    Initialized = true;
                }

                Shop.Open();
            }

            await Task.FromResult(0);
        }
    }
}
