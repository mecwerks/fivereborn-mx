﻿using System;
using System.Threading.Tasks;

using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System.Collections.Generic;
using System.Drawing;
using NativeUI;

namespace mxfivereborn
{
    public class MXScript : BaseScript
    {
        private bool initialized = false;

        //
        //
        //
        public MXScript()
        {
            RegisterEventHandlers();

            Tick += OnTick;
        }

        //
        //
        //
        private async Task OnTick()
        {
            await Task.FromResult(0);
        }

        //
        // Event Handlers
        //
        public void RegisterEventHandlers()
        {
            EventHandlers["onClientResourceStart"] += new Action<string>(resourceStarted);
            EventHandlers["onClientResourceStop"] += new Action<string>(resourceStopped);

            EventHandlers["mx:playerSpawned"] += new Action(playerSpawned);
            EventHandlers["mx:playerFirstSpawned"] += new Action(playerFirstSpawned);
            EventHandlers["baseevents:onPlayerDied"] += new Action<int, dynamic>(playerDied);
            EventHandlers["baseevents:onPlayerKilled"] += new Action<int, dynamic>(playerKilled);
            EventHandlers["baseevents:onPlayerWasted"] += new Action<dynamic>(playerWasted);
        }

        public void resourceStarted(string name)
        {
            if (name != "mxfreeroam" && initialized)
                Screen.ShowNotification($"{Colors.sGreen}Resource Started: {Colors.sDefault}{name}");
        }

        public void resourceStopped(string name)
        {
            if (initialized)
                Screen.ShowNotification($"{Colors.sRed}Resource Stopped: {Colors.sDefault}{name}");
        }

        //
        //
        //
        public void playerFirstSpawned()
        {
            initialized = true;
            Screen.ShowNotification($"Welcome to {Colors.sBlue} Marxy's {Colors.sDefault} Server!", true);
        }

        public void playerSpawned()
        {
            LocalPlayer.Character.Weapons.Give(WeaponHash.AssaultRifle, 200, false, true);
            LocalPlayer.Character.Weapons.Give(WeaponHash.Pistol, 200, false, true);
            LocalPlayer.Character.Weapons.Give(WeaponHash.SniperRifle, 200, false, true);
            LocalPlayer.Character.Weapons.Give(WeaponHash.Minigun, 200, false, true);
            LocalPlayer.Character.Weapons.Give(WeaponHash.Railgun, 200, false, true);
            LocalPlayer.Character.Weapons.Select(WeaponHash.Unarmed, true);
        }

        public void playerDied(int killerType, dynamic position)
        {
            Screen.ShowNotification($"You {Colors.sRed} Died.");
        }

        public void playerKilled(int killerid, dynamic info)
        {
            //
        }

        public void playerWasted(dynamic position)
        {
            //
        }
    }
}