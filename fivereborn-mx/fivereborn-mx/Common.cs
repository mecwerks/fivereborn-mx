﻿using System;

using CitizenFX.Core;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core.Native;

namespace mxfivereborn
{
    public class MapLocations
    {
        public List<Vector3> ConvienienceStores = new List<Vector3>()
        {
            new Vector3(-1486.71f, -381.852f, 40.1634f),
            new Vector3(-1224.04f, -906.344f, 12.3264f),
            new Vector3(-711.191f, -912.376f, 19.2156f),
            new Vector3(29.5184f, -1344.23f, 29.497f),
            new Vector3(-48.8503f, -1753.41f, 29.421f),
            new Vector3(1159.23f, -322.147f, 69.205f),
            new Vector3(1138.09f, -981.366f, 46.4158f),
            new Vector3(378.322f, 327.247f, 103.566f),
            new Vector3(1699.92f, 4927.63f, 42.0637f),
            new Vector3(1965.32f, 3739.66f, 31.6488f),
            new Vector3(-2974.7f, 390.901f, 15.0319f),
            new Vector3(2683.1f, 3282.11f, 55.2406f),
        };

        public List<Vector3> ClothingStores = new List<Vector3>()
        {
            // discount clothing
            new Vector3(80.665f, -1391.67f, 34.8334f),
            new Vector3(1687.98f, 4820.54f, 45.9631f),
            new Vector3(-1098.12f, 2709.18f, 19.1079f),
            new Vector3(1197.97f, 2704.22f, 43.0591f),
            new Vector3(-0.823129f, 6514.77f, 36.1644f),
            // bianco clothing
            new Vector3(-818.52f, -1077.54f, 15.4707f),
            new Vector3(419.633f, -809.586f, 36.2622f),
            // suburban clothing
            new Vector3(-1199.71f, -776.697f, 26.2131f),
            new Vector3(618.287f, 2752.56f, 48.5239f),
            new Vector3(126.786f, -212.513f, 59.8468f),
            new Vector3(-3168.87f, 1055.28f, 27.6547f),
        };

        public List<Vector3> ModShops = new List<Vector3>()
        {
            new Vector3(-1145.29f, -1991.23f, 13.1622f),
            new Vector3(723.129f, -1089.07f, 31.1061f),
            new Vector3(-354.526f, -135.274f, 59.9924f),
            new Vector3(1174.8f, 2644.42f, 43.5054f),
            new Vector3(110.046f, 6623.03f, 39.2303f), //beekers
            new Vector3(-207.123f, -1310.3f, 31.296f), //bennys shop
        };

        public List<Vector3> TattooShops = new List<Vector3>()
        {
            new Vector3(321.737f, 179.474f, 127.758f),
            new Vector3(1861.78f, 3750.06f, 37.2415f),
            new Vector3(-290.16f, 6199.09f, 35.6482f),
            new Vector3(-1156.55f, -1420.59f, 1126.49f),
            new Vector3(1322.4f, -1651.04f, 57.4641f),
            new Vector3(-3196.48f, 1074.81f, 25.4839f),
        };

        public List<Vector3> GunShops = new List<Vector3>()
        {
            new Vector3(1697.88f, 3753.23f, 39.1827f),
            new Vector3(244.749f, -45.6379f, 83.3083f),
            new Vector3(843.485f, -1025.05f, 37.0438f),
            new Vector3(-325.984f, 6077.07f, 37.1471f),
            new Vector3(-664.477f, -944.667f, 21.7855f),
            new Vector3(-1313.87f, -391.032f, 43.3075f),
            new Vector3(-1112.89f, 2690.44f, 18.5839f),
            new Vector3(-3165.23f, 1082.86f, 25.4796f),
            new Vector3(2569.69f, 302.509f, 117.451f),
            new Vector3(812.434f, -2148.97f, 39.4198f), //shooting range
            new Vector3(17.7044f, -1114.19f, 42.373f), //shooting range
        };

        public List<Vector3> BarberShops = new List<Vector3>()
        {
            new Vector3(-822.051f, -187.093f, 48.1654f),
            new Vector3(133.515f, -1710.83f, 36.5914f),
            new Vector3(-1287.14f, -1116.47f, 10.1592f),
            new Vector3(1933.07f, 3726.17f, 36.7415f),
            new Vector3(1208.28f, -470.83f, 71.8597f),
            new Vector3(-30.4352f, -147.733f, 62.3003f),
            new Vector3(-280.715f, 6231.77f, 37.1876f),
        };
    }

    public class Colors
    {
        public static Color Red = Color.FromArgb(255, 20, 20, 255);
        public static Color Green = Color.FromArgb(20, 255, 20, 255);
        public static Color Blue = Color.FromArgb(20, 20, 255, 255);
        public static Color Gray = Color.FromArgb(45, 45, 45, 255);
        public static Color Black = Color.FromArgb(0, 0, 0, 255);
        public static Color White = Color.FromArgb(255, 255, 255, 255);

        public static string sRed = "~r~";
        public static string sBlue = "~b~";
        public static string sGreen = "~g~";
        public static string sYellow = "~y~";
        public static string sPurple = "~p~";
        public static string sOrange = "~o~";
        public static string sGrey = "~c~";
        public static string sDarkerGray = "~m~";
        public static string sBlack = "~u~";
        public static string sDefault = "~s~";
        public static string sWhite = "~w~";
        public static string sBold = "~h~";
        public static string sRockstar = "÷";
        public static string sRockstar2 = "∑";
        public static string sVerified = "¦";
    }

    public class Util
    {
        public static IEnumerable<T> GetEnumValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static void Print(dynamic message)
        {
            Debug.Write("MXFreeroam: " + message.ToString() + "\n");
        }

        public static string GetVehicleName(Model veh)
        {
            return Game.GetGXTEntry(Vehicle.GetModelDisplayName(veh));
        }

        public static VehicleClass GetVehicleClass(VehicleHash veh)
        {
            return (VehicleClass)Function.Call<uint>(Hash.GET_VEHICLE_CLASS_FROM_NAME, veh);
        }

        public static string GetVehicleClassName(VehicleHash veh)
        {
            return GetVehicleClassName(GetVehicleClass(veh));
        }

        public static string GetVehicleClassName(VehicleClass vehClass)
        {
            return Game.GetGXTEntry(Vehicle.GetClassDisplayName(vehClass));
        }
    }
}
